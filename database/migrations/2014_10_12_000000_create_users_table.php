<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('title');
            $table->string('surname');
            $table->string('firstname');
            $table->string('lastname');
            $table->timestamp('joined_date')->nullable();
            $table->datetime('last_login');
            $table->string('country');
            $table->string('rfc');
            $table->string('cp');
            $table->string('razon_social');
            $table->string('domicilio');
            $table->string('exterior');
            $table->string('interior');
            $table->string('dleg');
            $table->string('colonia');
            $table->string('localidad');
            $table->integer('status');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
