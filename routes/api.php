<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('products/{id}','API\ProductController@index');
Route::get('categories','API\CategoryController@categories');
Route::get('subcategory/{id}','API\CategoryController@subcategory');
Route::get('product_images/{id}','API\ProductController@product_images');
Route::get('product_details/{id}','API\ProductController@product_details');

Route::middleware('auth:api')->group(function(){
	Route::resource('books','API\BookController');
});

Route::post('register','API\Auth\RegisterController@register');
