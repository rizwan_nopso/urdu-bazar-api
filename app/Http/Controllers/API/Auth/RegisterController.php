<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Validator;

class RegisterController extends BaseController
{

    public function register(Request $request){
    	// $input = $request->all();
    	$validator = Validator::make($request->all(), [
    		'surname'		=>	'required',
    		'email'		=>	'required|email|unique:users',
    		'password'	=>	'required',
    		// 'c_password'	=>	'required|same:password',
    	]);

    	if($validator->fails()){
    		return $this->sendError('error validation', $validator->errors());
    	}

    	$input = $request->all();
    	$input['password'] = bcrypt($input['password']);
    	$user = User::create($input);
    	$success['token'] = $user->createToken('MyApp')->accessToken;
    	$success['surname'] = $user->surname;

    	return $this->sendResponse($success, 'User created successfully');
    }
}
