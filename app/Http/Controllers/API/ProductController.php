<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use app\Models\ProductImage;

class ProductController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index($id){
        $products = new Product();
        return response()->json($products->products($id));
    }
    public function product_images($id){
    	$productsImages = new ProductImage();
    	return response()->json($productsImages->productImages($id));
    }
    public function product_details($id){
    	$product_details = new Product();
    	return response()->json($product_details->product_details($id));
    }
    
}
