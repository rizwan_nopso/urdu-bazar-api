<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Session;
use DB;

class CategoryController extends Controller
{
    
    public function categories(){
        $categories = new Category();
        return response()->json($categories->categories());	
    }
    public function subcategory($id){
        $categories = new Category();
        // return response::json($categories->subcategory($id));
        return response()->json($categories->subcategory($id));
    }
}
