<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Product extends Model
{
    protected $table = 'products';
    protected $primeryKey = 'product_id';
      
    protected $guarded = [];

    public function products($id){
        $subcategory = Product::where('cat_id',$id)->get();
        return $subcategory;
    }
    public function product_details($id){
    	$product_details = Product::where('product_id',$id)->first();
    	return $product_details;
    }
}
