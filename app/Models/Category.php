<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use DB;

class Category extends Model
{
    //

    protected $table = 'categories';
  	protected $primeryKey = 'category_id';
  	protected $guarded = [];

  	public function categories(){
  		$categories = [];
  		$categories['data'] = Category::get();
  		return $categories;
  	}
  	public function subcategory($id){
  		$subcategory = Category::where('category_id',$id)->orWhere('parent',$id)->get();
  		return $subcategory;
  	}
}
