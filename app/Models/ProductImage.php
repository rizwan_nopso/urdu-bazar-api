<?php

namespace App\Models;
use app\Models\ProductImage;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
     protected $table = 'product_images';
     protected $primeryKey = 'pro_image_id';
     protected $guarded = [];

     public function productImages($id){
     	$productImages = ProductImage::where('product_id',$id)->get();

     	return $productImages;
     }
}
